#include<iostream>
#include<stdio.h>
#include<conio.h>
#include<cstdlib>
using namespace std;


struct node
{
   int data;
   struct node *next;
};


class stack
{
   struct node *top;
   public:
      stack() // CONSTRUCTOR
      {
	 top=NULL;
      }
      void push();
      void pop();
      void show();
      ~stack();
};
stack::~stack() // DESTRUCTION
{
      delete top;
}
void stack::push()
{
   int value;
   struct node *ptr;
   cout<<"\nPUSH Operation\n";
   cout<<"Enter a number to insert: ";
   cin>>value;
   ptr=new node;
   ptr->data=value;
   ptr->next=NULL;
   if(top!=NULL)
      ptr->next=top;
   top=ptr;

   getch();
}

void stack::pop()
{
   struct node *temp;
   if(top==NULL)
   {
      cout<<"\n EMPTY...";
      getch();
      return;
   }
   temp=top;
   top=top->next;
   cout<<"Poped value is "<<temp->data;
   delete temp;
   getch();
}


void stack::show()
{
   struct node *ptr1=top;
   cout<<"\nThe stack  \n";
   while(ptr1!=NULL)
   {
      cout<<ptr1->data<<" ->";
      ptr1=ptr1->next;
   }
   cout<<"EMPTY\n";
   getch();
}


int main()
{
   stack s;
   int choice;
   while(1)
   {
      cout<<"\n1:PUSH\n2:POP\n3:DISPLAY \n4:EXIT"; /* CHOICE */
      cout<<"\n  your choice IS...: ";
      cin>>choice;
      switch(choice) /* USING SWITCH FOR CHOICE*/
      {
       case 1:
	  s.push();
	  break;
       case 2:
	  s.pop();
	  break;
       case 3:
	  s.show();
	  break;
       case 4:
	  exit(0);
	  break;
       default:
	  cout<<" ENTER YOUR CHOICE...";
	  getch();
	  break;
       }
   }
   return 0;
}
